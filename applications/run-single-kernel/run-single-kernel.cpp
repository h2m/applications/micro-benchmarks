#include "h2m.h"
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <omp.h>

#include "../../src/data_container.h"
#include "../../src/kernel.h"

#ifndef DATA_CONTAINER_TYPE
#error "No data container type specified."
#endif
#ifndef DATA_CONTAINER_HEADER
#error "No data container header file specified."
#endif
#include DATA_CONTAINER_HEADER

#ifndef KERNEL_TYPE
#error "No kernel type specified."
#endif
#ifndef KERNEL_HEADER
#error "No kernel header file specified."
#endif
#include KERNEL_HEADER

double calc_median(std::vector<double> vec) {
    size_t size = vec.size();
    if (size == 0) {
        return 0;  // Undefined, really.
    } else {
        sort(vec.begin(), vec.end());
        if (size % 2 == 0) {
            return (vec[size / 2 - 1] + vec[size / 2]) / 2;
        }
        else {
            return vec[size / 2];
        }
    }
}

int main(int argc, char **argv) {
    double time_elapsed;
    int err;

    size_t niters = 10;
    // get number of iterations from env variable
    char *tmp = nullptr;
    tmp = std::getenv("N_ITERS");
    if(tmp) {
        niters = std::atoi(tmp);
    }
    printf("Running kernel with %zu iterations ...\n", niters);

    bool run_placement_check = false;
    tmp = nullptr;
    tmp = std::getenv("PLACEMENT_CHECK");
    if(tmp) {
        run_placement_check = std::atoi(tmp) == 1;
    }

    // init H2M library
    h2m_init();
    #pragma omp parallel
    {
        h2m_thread_init();
    }

    // data container and kernel objects
    data_container_t *dc;
    kernel_t *kernel;

    // initialize data container
    dc = new DATA_CONTAINER_TYPE();
    dc->init(nullptr);
    // init kernel
    kernel = new KERNEL_TYPE();
    kernel->init(dc, nullptr);
    kernel->set_traits_for_data();

    // allocate data now
    dc->allocate(true);

    // commit if necessary
    if(dc->declarations.size() > 0) {
        err = h2m_commit_allocs(&(dc->declarations[0]), dc->declarations.size());
        if(err != H2M_SUCCESS) {
            fprintf(stderr, "Error: h2m_commit_allocs returned %d\n", err);
            return err;
        }
    }

    // run data initialization
    dc->data_init(true);

    printf("Data Container: %s\n", dc->get_id().c_str());
    printf("Kernel: %s\n", kernel->get_id().c_str());

    if(run_placement_check) {
        bool okay = dc->check_data_placement();
        if(!okay) {
            fprintf(stderr, "Error: data placement not correct!\n");
            return H2M_FAILURE;
        }
    }

    std::vector<double> times;
    for (size_t it = 0; niters > it; ++it) {
        // run the kernel
        time_elapsed = omp_get_wtime();
        kernel->run(true);
        time_elapsed = omp_get_wtime() - time_elapsed;
        times.push_back(time_elapsed);
        if(it == 0) {
            // run verification
            bool pass = kernel->verify();
            if (pass) {
                printf("Kernel: %s - Verification: SUCCESS\n", kernel->get_id().c_str());
            } else {
                printf("Kernel: %s - Verification: FAILED\n", kernel->get_id().c_str());
            }
        }
    }
    sort(times.begin(), times.end());
    time_elapsed = 0.;
    for (double t : times) {
        time_elapsed += t;
    }

    printf("Results elapsed execution time:\n");
    printf("\tavg:\t%f\tsec\n\tmin:\t%f\tsec\n\tmed:\t%f\tsec\n\tmax:\t%f\tsec\n", time_elapsed/times.size(), times.front(), calc_median(times), times.back());

    // clean up
    dc->finalize();
    kernel->finalize();

    delete dc;
    delete kernel;

    #pragma omp parallel
    {
        h2m_thread_finalize();
    }
    h2m_finalize();

    return 0;
}
