#include "h2m.h"
#include "h2m_pre_init.h"

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <iostream>
#include <omp.h>
#include <vector>

#include "data_container.h"
#include "data_container_3mat.h"

#include "kernel.h"
#include "kernel_dot_prod.h"
#include "kernel_dot_prod_transposedB.h"
#include "kernel_MxNxK.h"
#include "kernel_MxNxK_transposedB.h"

#ifndef N_KERNEL_MXM
#define N_KERNEL_MXM 5
#endif

#ifndef N_KERNEL_MXM_TRANSPOSED_B
#define N_KERNEL_MXM_TRANSPOSED_B 4
#endif

#ifndef N_KERNEL_DOT
#define N_KERNEL_DOT 3
#endif

#ifndef N_KERNEL_DOT_TRANSPOSED_B
#define N_KERNEL_DOT_TRANSPOSED_B 2
#endif

#ifndef N_ITERS
#define N_ITERS 1
#endif

#ifndef PAR_MODE
#define PAR_MODE 0
#endif

#ifndef BASE_SIZE_MB_MATRIX_MXM
#define BASE_SIZE_MB_MATRIX_MXM 32
#endif

#ifndef BASE_SIZE_MB_MATRIX_DOT
#define BASE_SIZE_MB_MATRIX_DOT 64
#endif

#define STRINGFY(x) #x

int n_kernel_dot = N_KERNEL_DOT;
int n_kernel_dot_transposed_B = N_KERNEL_DOT_TRANSPOSED_B;
int n_kernel_mxm = N_KERNEL_MXM;
int n_kernel_mxm_transposed_B = N_KERNEL_MXM_TRANSPOSED_B;

int niters = N_ITERS;
int parallel_mode = PAR_MODE;
size_t base_size_mb_matrix_mxm = BASE_SIZE_MB_MATRIX_MXM;
size_t base_size_mb_matrix_dot = BASE_SIZE_MB_MATRIX_DOT;

size_t sum_bytes_total = 0;
size_t ctr_kernels = 0;
std::vector<data_container_3mat_t*> list_containers;
std::vector<kernel_t*> list_kernels;
std::vector<h2m_declaration_t> list_all_declarations;

int parse_command_line_args(int argc, char **argv) {
    if(argc > 1) {
        if (strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0) {
            std::cout << "Usage: ./run-multiple-kernels.exe <n_mxm> <n_mxm_tb> <n_dot> <n_dot_tb>" << std::endl;
            std::cout << "  Optional positional arguments: " << std::endl;
            std::cout << "    n_mxm:     Number of DGEMM kernels with all linear access pattern" << std::endl;
            std::cout << "    n_mxm_tb:  Number of DGEMM kernels with transposed B matrix" << std::endl;
            std::cout << "    n_dot:     Number of dot_prod kernels with all linear access pattern" << std::endl;
            std::cout << "    n_dot_tb:  Number of dot_prod kernels with transposed B matrix" << std::endl;
            return 1;
        }
        n_kernel_mxm = atoi(argv[1]);
    }

    if(argc > 2) {
        n_kernel_mxm_transposed_B = atoi(argv[2]);
    }

    if(argc > 3) {
        n_kernel_dot = atoi(argv[3]);
    }

    if(argc > 4) {
        n_kernel_dot_transposed_B = atoi(argv[4]);
    }

    return 0;
}

void parse_env_vars() {
    char *tmp = nullptr;

    tmp = nullptr;
    tmp = std::getenv("N_ITERS");
    if(tmp) {
        niters = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("PAR_MODE");
    if(tmp) {
        parallel_mode = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("BASE_SIZE_MB_MATRIX_MXM");
    if(tmp) {
        base_size_mb_matrix_mxm = (size_t) std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("BASE_SIZE_MB_MATRIX_DOT");
    if(tmp) {
        base_size_mb_matrix_dot = (size_t) std::atoi(tmp);
    }    
}

double calc_median(std::vector<double> vec) {
    size_t size = vec.size();
    if (size == 0) {
        return 0;  // Undefined, really.
    } else {
        sort(vec.begin(), vec.end());
        if (size % 2 == 0) {
            return (vec[size / 2 - 1] + vec[size / 2]) / 2;
        }
        else {
            return vec[size / 2];
        }
    }
}

template<typename T>
void create_3mat_kernel(const char* kernel_name, settings_3mat_t &tmp_dc_sett, size_t size_matrix, size_t size_bytes) {
    // increment total used memory
    sum_bytes_total += size_bytes * 3;
    printf("Creating kernel[%03zu]: %s with size %d x %d (%f MB per matrix)\n", ctr_kernels, kernel_name, size_matrix, size_matrix, ((double)size_bytes / 1000. / 1000.));

    // init data container
    data_container_3mat_t *dc = new data_container_3mat_t();
    dc->init(&tmp_dc_sett);
    list_containers.push_back(dc);

    // init kernel
    T *tmp_kernel = new T();
    tmp_kernel->init(dc, nullptr);
    tmp_kernel->set_traits_for_data();
    list_kernels.push_back(tmp_kernel);
    ctr_kernels++;

    // declare data allocation
    dc->allocate(true);
    // gather all declarations
    list_all_declarations.insert(std::end(list_all_declarations), std::begin(dc->declarations), std::end(dc->declarations));
}

int main(int argc, char **argv) {

    // parse command line arguments
    int ret_code = parse_command_line_args(argc, argv);
    if (ret_code != 0) {
        return ret_code;
    }
    parse_env_vars();

    // library initialization
    h2m_pre_init();
    #pragma omp parallel
    {
        h2m_thread_init();
    }

    int err;
    double time_create;
    double time_commit_allocate;
    double time_data_init;
    double time_run;
    double time_kernel;

    std::vector<int> list_strides = {1};

    time_create = omp_get_wtime();
    // create MxM kernels
    for(int i = 0; i < n_kernel_mxm; i++) {
        size_t tmp_size_bytes = (base_size_mb_matrix_mxm*1000*1000) * (i+1);
        size_t tmp_size_matrix = (size_t) sqrt((double)tmp_size_bytes / sizeof(double));

        settings_3mat_t tmp_dc_sett;
        tmp_dc_sett.size_M = tmp_dc_sett.size_N = tmp_dc_sett.size_K = tmp_size_matrix;

        create_3mat_kernel<kernel_MxNxK_t>(STRINGFY(kernel_MxNxK_t), tmp_dc_sett, tmp_size_matrix, tmp_size_bytes);
    }
    for(int i = 0; i < n_kernel_mxm_transposed_B; i++) {
        size_t tmp_size_bytes = (base_size_mb_matrix_mxm*1000*1000) * (i+1);
        size_t tmp_size_matrix = (size_t) sqrt((double)tmp_size_bytes / sizeof(double));

        settings_3mat_t tmp_dc_sett;
        tmp_dc_sett.size_M = tmp_dc_sett.size_N = tmp_dc_sett.size_K = tmp_size_matrix;

        create_3mat_kernel<kernel_MxNxK_transposedB_t>(STRINGFY(kernel_MxNxK_transposedB_t), tmp_dc_sett, tmp_size_matrix, tmp_size_bytes);
    }

    // create dot prod kernels
    for(int i = 0; i < n_kernel_dot; i++) {
        size_t tmp_size_bytes = (base_size_mb_matrix_dot*1000*1000) * (i+1);
        size_t tmp_size_matrix = (size_t) sqrt((double)tmp_size_bytes / sizeof(double));

        settings_3mat_t tmp_dc_sett;
        tmp_dc_sett.size_M = tmp_dc_sett.size_N = tmp_dc_sett.size_K = tmp_size_matrix;        

        for(int j = 0; j < list_strides.size(); j++) {
            settings_dot_prod_t tmp_k_sett;
            tmp_k_sett.stride = list_strides[j];

            create_3mat_kernel<kernel_dot_prod_t>(STRINGFY(kernel_dot_prod_t), tmp_dc_sett, tmp_size_matrix, tmp_size_bytes);
        }
    }
    for(int i = 0; i < n_kernel_dot_transposed_B; i++) {
        size_t tmp_size_bytes = (base_size_mb_matrix_dot*1000*1000) * (i+1);
        size_t tmp_size_matrix = (size_t) sqrt((double)tmp_size_bytes / sizeof(double));

        settings_3mat_t tmp_dc_sett;
        tmp_dc_sett.size_M = tmp_dc_sett.size_N = tmp_dc_sett.size_K = tmp_size_matrix;

        for(int j = 0; j < list_strides.size(); j++) {
            settings_dot_prod_t tmp_k_sett;
            tmp_k_sett.stride = list_strides[j];

            create_3mat_kernel<kernel_dot_prod_transposedB_t>(STRINGFY(kernel_dot_prod_transposedB_t), tmp_dc_sett, tmp_size_matrix, tmp_size_bytes);
        }
    }
    time_create = omp_get_wtime() - time_create;

    printf("Created %zu micro kernel tasks. Running %d iterations. Overall data amount: %.2f MB\n", list_kernels.size(), niters, ((double)sum_bytes_total / 1000.0 / 1000.0));

    time_commit_allocate = omp_get_wtime();
    if(list_all_declarations.size() > 0) {
        err = h2m_commit_allocs(&(list_all_declarations[0]), list_all_declarations.size());
        if(err != H2M_SUCCESS) {
            fprintf(stderr, "Error: h2m_commit_allocs returned %d\n", err);
            return err;
        }
    }
    time_commit_allocate = omp_get_wtime() - time_commit_allocate;

    // run data initialization
    time_data_init = omp_get_wtime();
    if(parallel_mode == 0) {
        // Option 1: kernels are executed sequentially but parallel inside
        for(int i = 0; i < list_containers.size(); i++) {
            list_containers[i]->data_init(true);
        }
    } else {
        // Option 2: kernels are executed in parallel but sequentially inside
        #pragma omp parallel for schedule(static,1)
        for(int i = 0; i < list_containers.size(); i++) {
            list_containers[i]->data_init(false);
        }
    }
    time_data_init = omp_get_wtime() - time_data_init;

    // check where data has been allocated in the end
    int found;
    printf("Resulting placement:\n");
    for(int i = 0; i < list_containers.size(); i++) {
        {
            h2m_alloc_info_t info = h2m_get_alloc_info(list_containers[i]->A, &found);
            if(!found) {
                fprintf(stderr, "Error: Alloc info not found for kernel[%03d] A=%p\n", i, list_containers[i]->A);
            }
            char str_space[64];
            char str_partition[64];
            h2m_alloc_trait_value_t_str(info.cur_mem_space, str_space);
            h2m_alloc_trait_value_t_str(info.cur_mem_partition, str_partition);
            printf("Kernel[%03d] A=%p, size=%11zu, prio=%6.2f, mem_space=%30s,\tmem_partition=%s,\tnodemask=%lu\n", i, list_containers[i]->A, info.size, info.traits[1].value.dbl, str_space, str_partition, info.nodemask);
        }
        {
            h2m_alloc_info_t info = h2m_get_alloc_info(list_containers[i]->B, &found);
            if(!found) {
                fprintf(stderr, "Error: Alloc info not found for B=%p\n", list_containers[i]->B);
            }
            char str_space[64];
            char str_partition[64];
            h2m_alloc_trait_value_t_str(info.cur_mem_space, str_space);
            h2m_alloc_trait_value_t_str(info.cur_mem_partition, str_partition);
            printf("Kernel[%03d] B=%p, size=%11zu, prio=%6.2f, mem_space=%30s,\tmem_partition=%s,\tnodemask=%lu\n", i, list_containers[i]->B, info.size, info.traits[1].value.dbl, str_space, str_partition, info.nodemask);
        }
        {
            h2m_alloc_info_t info = h2m_get_alloc_info(list_containers[i]->C, &found);
            if(!found) {
                fprintf(stderr, "Error: Alloc info not found for C=%p\n", list_containers[i]->C);
            }
            char str_space[64];
            char str_partition[64];
            h2m_alloc_trait_value_t_str(info.cur_mem_space, str_space);
            h2m_alloc_trait_value_t_str(info.cur_mem_partition, str_partition);
            printf("Kernel[%03d] C=%p, size=%11zu, prio=%6.2f, mem_space=%30s,\tmem_partition=%s,\tnodemask=%lu\n", i, list_containers[i]->C, info.size, info.traits[1].value.dbl, str_space, str_partition, info.nodemask);
        }
    }

    std::vector<double> times_kernels(list_kernels.size(), 0.);
    std::vector<double> times;
    for (size_t it = 0; niters > it; ++it) {
        // run the kernel
        time_run = omp_get_wtime();

        if(parallel_mode == 0) {
            // Option 1: kernels are executed sequentially but parallel inside
            for(int k = 0; k < list_kernels.size(); k++) {
                time_kernel = omp_get_wtime();
                list_kernels[k]->run(true);
                time_kernel = omp_get_wtime() - time_kernel;
                times_kernels[k] += time_kernel;
            }
        } else {
            // Option 2: kernels are executed in parallel but sequentially inside
            #pragma omp parallel for schedule(static,1)
            for(int k = 0; k < list_kernels.size(); k++) {
                list_kernels[k]->run(false);
            }
        }

        time_run = omp_get_wtime() - time_run;
        times.push_back(time_run);
    }

    sort(times.begin(), times.end());
    time_run = 0.;
    for (double t : times) {
        time_run += t;
    }
    printf("Elapsed time for task/kernel creation: %f sec\n", time_create);
    printf("Elapsed time for committing allocation declarations: %f sec\n", time_commit_allocate);
    printf("Elapsed time for data init: %f sec\n", time_data_init);
    printf("Elapsed time for overall execution:\n");
    printf("\tavg:\t%f\tsec\n\tmin:\t%f\tsec\n\tmed:\t%f\tsec\n\tmax:\t%f\tsec\n", time_run/times.size(), times.front(), calc_median(times), times.back());

    // clean up
    for(int i = 0; i < list_kernels.size(); i++) {
        printf("Elapsed time for kernel[%03d]:\t%f\tsec\n", i, times_kernels[i]);
        list_containers[i]->finalize();
        list_kernels[i]->finalize();
        delete list_containers[i];
        delete list_kernels[i];
    }

    for(int i = 0; i < list_all_declarations.size(); i++) {
        err = h2m_declaration_free(list_all_declarations[i]);
    }

    #pragma omp parallel
    {
        h2m_thread_finalize();
    }
    h2m_finalize();

    return 0;
}
