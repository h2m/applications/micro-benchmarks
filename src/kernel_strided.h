#ifndef __KERNEL_STRIDED_H__
#define __KERNEL_STRIDED_H__

#include "kernel.h"
#include "data_container.h"
#include "data_container_array.h"
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>

#ifndef N_REPS
#define N_REPS 1
#endif

#ifndef STRIDE
#define STRIDE 1
#endif

#ifndef VEC_LEN
#define VEC_LEN 8
#endif

#ifndef ACCESS_MODE
#define ACCESS_MODE 0   // 0:Write, 1=Read
#endif

class settings_strided_t {
    public:
        int n_reps;
        int stride;
        int access_mode;

        // default constructor
        settings_strided_t () {
            n_reps = N_REPS;
            stride = STRIDE;
            access_mode = ACCESS_MODE;
        }

        // copy constructor
        settings_strided_t(const settings_strided_t& other) {
            n_reps = other.n_reps;
            stride = other.stride;
            access_mode = other.access_mode;
        }
};

class kernel_strided_t : public kernel_t {

    protected:
        data_container_array_t *dc;
        settings_strided_t sett;

    public:
        void init(data_container_t *dc, void *args) {
            this->dc = static_cast<data_container_array_t *>(dc);

            if(args) {
                // copy values to internal settings
                settings_strided_t *other = static_cast<settings_strided_t*>(args);
                this->sett = *other;
            }
        }

        void set_traits_for_data() {
            // TODO/FIXME: implement
            data_container_array_t *dco = this->dc;

            // TODO/FIXME: implement proper traits
            // hard coded traits here for now?

            //
            // Update traits if the keys already exist
            //
            const double prio = 1.;
            const h2m_alloc_trait_value_t mem_space = h2m_atv_mem_space_hbw;

            // dco->sett.traits = ...;
            // dco->sett.n_traits = ...;
            h2m_alloc_trait_t traits[2] = { { h2m_atk_pref_mem_space, mem_space }, { h2m_atk_access_prio, prio } };
            dco->sett.update_traits(2, traits_A);

            // TODO/FIXME: update traits for data structures (might not be successful before allocation but should not hurt)
            h2m_update_traits(dco->arr, dco->sett.size_array * sizeof(double), dco->sett.n_traits, dco->sett.traits, 1);
        }

        int run(bool parallel) {
            settings_array_t *ds = &(this->dc->sett);

            volatile double keep = 0;
            volatile long ctr_iter = 0;
            volatile long ctr_reps = 0;
            double val = 0.34;
            double mult = 17.41;
            double *arr = this->dc->arr;

            if(this->sett.access_mode == 0) { // write-only
                if(this->sett.stride == 1) {
                    for(int rep = 0; rep < this->sett.n_reps; rep++) {
                        ctr_reps += rep; // needed to avoid interchange of loops
                        #pragma omp parallel for schedule(static) if(parallel)
                        for(int j = 0; j < ds->size_array; j+=VEC_LEN) {
                            #pragma omp simd aligned(arr)
                            for(int j2 = j; j2 < j+VEC_LEN; j2++) {
                                arr[j2] += val * mult;
                            }
                        }
                    }
                } else {
                    // ensure the benchmark does roughly the same amount of accesses compared to stride=1
                    for(int rep = 0; rep < this->sett.n_reps; rep++) {
                        ctr_reps += rep; // needed to avoid interchange of loops
                        for(int it = 0; it < this->sett.stride; it++) {
                            ctr_iter += it;
                            #pragma omp parallel for schedule(static) if(parallel)
                            for(int j = 0; j < ds->size_array; j+=VEC_LEN) {
                                #pragma omp simd aligned(arr)
                                for(int j2 = j; j2 < j+VEC_LEN; j2+=this->sett.stride) {
                                    arr[j2] += val * mult;
                                }
                            }
                        }
                    }
                }
            } else { // read-only
                if(this->sett.stride == 1) {
                    for(int rep = 0; rep < this->sett.n_reps; rep++) {
                        ctr_reps += rep; // needed to avoid interchange of loops
                        #pragma omp parallel for schedule(static) if(parallel)
                        for(int j = 0; j < ds->size_array; j+=VEC_LEN) {
                            #pragma omp simd aligned(arr)
                            for(int j2 = j; j2 < j+VEC_LEN; j2++) {
                                val += arr[j2] * mult;
                            }
                        }
                    }
                } else {
                    // ensure the benchmark does roughly the same amount of accesses compared to stride=1
                    for(int rep = 0; rep < this->sett.n_reps; rep++) {
                        ctr_reps += rep; // needed to avoid interchange of loops
                        for(int it = 0; it < this->sett.stride; it++) {
                            ctr_iter += it;
                            #pragma omp parallel for schedule(static) if(parallel)
                            for(int j = 0; j < ds->size_array; j+=VEC_LEN) {
                                #pragma omp simd aligned(arr)
                                for(int j2 = j; j2 < j+VEC_LEN; j2+=this->sett.stride) {
                                    val += arr[j2] * mult;
                                }
                            }
                        }
                    }
                }
            }

            // dummy calculation
            if (val != 0.34) val = 0.34;
            keep = ctr_iter + ctr_reps + val;

            return (int)keep;
        }

        bool verify() {
            // skip validation here
            return true;
        }

        std::string get_id() {
            std::string ret = "kernel_strided_access-mode_" + std::to_string(this->sett.access_mode) + "_stride_" + std::to_string(this->sett.stride) +  "_rep_" + std::to_string(this->sett.n_reps);
            return ret;
        }

        void finalize() { }
};

#endif // __KERNEL_STRIDED_H__
