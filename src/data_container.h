#ifndef __DATA_CONTAINER_H__
#define __DATA_CONTAINER_H__

#include "h2m.h"

#include <cstdlib>
#include <string>
#include <vector>

class data_container_t {

    public:

        data_container_t() {}

        std::vector<h2m_declaration_t> declarations;

        virtual void        init(void *args) = 0;
        virtual void        allocate(bool declare_alloc) = 0;
        virtual void        data_init(bool parallel) = 0;
        virtual bool        check_data_placement() = 0;
        virtual std::string get_id() = 0;
        virtual void        finalize() = 0;
        virtual            ~data_container_t() {}
};

#endif // __DATA_CONTAINER_H__
