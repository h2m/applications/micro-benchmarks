#ifndef __DATA_CONTAINER_ARRAY_H__
#define __DATA_CONTAINER_ARRAY_H__

#include "data_container.h"
#include <cassert>
#include <cstdio>
#include <cstdlib>

#ifndef SIZE_ARRAY
#define SIZE_ARRAY 80000000 // 640 MB
#endif
#ifndef VAL_INIT_ARRAY
#define VAL_INIT_ARRAY 1.0
#endif

#ifndef MEM_SPACE_ARR
#define _SPACE_ARR h2m_atv_mem_space_large_cap
#else
#  if   MEM_SPACE_ARR == HBW || MEM_SPACE_ARR == hbw
#    define _SPACE_ARR h2m_atv_mem_space_hbw
#  elif MEM_SPACE_ARR == LARGE_CAP || MEM_SPACE_ARR == large_cap
#    define _SPACE_ARR h2m_atv_mem_space_large_cap
#  elif MEM_SPACE_ARR == LOW_LAT || MEM_SPACE_ARR == low_lat
#    define _SPACE_ARR h2m_atv_mem_space_low_lat
#  else
#    error "Invalid memory space for Array"
#  endif
#endif

class settings_array_t {
    public:
        // dimension
        size_t size_array;

        // values for data init
        double val_array;

        // memory spaces
        h2m_alloc_trait_value_t mem_space;

        // traits
        h2m_alloc_trait_t *traits;
        int n_traits;

        // default constructor
        settings_array_t() {
            size_array = SIZE_ARRAY;
            val_array = VAL_INIT_ARRAY;
            mem_space = _SPACE_ARR;

            traits = nullptr;
            n_traits = 0;
        }

        // copy constructor
        settings_array_t(const settings_array_t& other) {
            size_array = other.size_array;
            val_array = other.val_array;
            mem_space = other.mem_space;

            traits = other.traits;
            n_traits = other.n_traits;
        }

        ~settings_3mat_t()
        {
            if (traits) delete traits;
        }

        void update_traits(int n_new_traits, h2m_alloc_trait_t *new_traits)
        {
            int new_n_traits = n_traits + n_new_traits;
            bool has_trait[n_new_traits] = { false };
            for (size_t i = 0; n_traits > i; ++i) {
                // Update trait if already existing
                for (size_t tid = 0; n_new_traits > tid; ++tid) {
                    if (traits[i].key == new_traits[tid].key) {
                        traits[i] = new_traits[tid];
                        has_trait[tid] = true;
                        new_n_traits--;
                        break;
                    }
                }
            }
            h2m_alloc_trait_t *t = new h2m_alloc_trait_t [new_n_traits];
            memcpy(t, traits, sizeof(h2m_alloc_trait_t [n_traits]));
            for (size_t tid = 0; n_new_traits > tid; ++tid) {
                if (!has_trait[tid])
                    t[n_traits++] = new_traits[tid];
            }
            std::swap(traits, t);
            delete t;
        }

};

class data_container_array_t : public data_container_t {

    public:
        settings_array_t sett;
        double *arr;

        void init(void *args) {
            // option to specify some parameters at run-time
            char *tmp = nullptr;
            tmp = nullptr; tmp = std::getenv("SIZE_ARRAY"); if(tmp) { this->sett.size_array = std::atol(tmp); }

            if(args) {
                // copy values to internal settings
                settings_array_t *other = static_cast<settings_array_t*>(args);
                this->sett = *other;
            }
        }

        void allocate(bool declare_alloc) {
            int err;

            // starting point: traits from settings
            int nt = this->sett.n_traits;
            h2m_alloc_trait_t *tr = this->sett.traits;

            // case: no traits specified -> use initial mem space for trait
            if(!this->sett.traits) {
                tr = new h2m_alloc_trait_t[1] { { .key = h2m_atk_pref_mem_space, .value = { .atv = this->sett.mem_space } } };
                nt = 1;
            }

            // allocate data
            if (declare_alloc) {
                this->declarations.push_back(h2m_declare_alloc_w_traits((void **)&(this->arr), this->sett.size_array * sizeof(double), nullptr, nullptr, &err, 1, tr));
            } else {
                this->arr = (double*) h2m_alloc_w_traits(this->sett.size_array * sizeof(double), &err, 1, tr);
            }

            // cleanup if temporary traits have been used
            if(!this->sett.traits) {
                delete[] tr;
            }
        }

        void data_init(bool parallel) {
            // for parallel init (assumption: number of threads specified via env variables)
            #pragma omp parallel for schedule(static) if(parallel)
            for(int i = 0; i < this->sett.size_array; i++) {
                this->arr[i] = this->sett.val_array;
            }
        }

        bool check_data_placement() {
            int found = 0;
            h2m_alloc_info_t info = h2m_get_alloc_info(this->arr, &found);
            assert(found==1);

            if(info.cur_mem_space != this->sett.mem_space) {
                return false;
            }

            // === DEBUG ===
            char str_space[64];
            h2m_alloc_trait_value_t_str(info.cur_mem_space, str_space);
            char str_partition[64];
            h2m_alloc_trait_value_t_str(info.cur_mem_partition, str_partition);
            printf("Placement Array: mem_space=%30s, mem_partition=%s, nodemask=%lu\n", str_space, str_partition, info.nodemask);
            // === DEBUG ===
            return true;
        }

        void finalize() {
            if(this->arr) {
                h2m_free(this->arr);
                this->arr = nullptr;
            }
        }

        std::string get_id() {
            std::string ret =
                "data_container_array_" +
                std::to_string(this->sett.size_array) + "_" +
                std::to_string(this->sett.mem_space);
            return ret;
        }
};

#endif // __DATA_CONTAINER_ARRAY_H__
