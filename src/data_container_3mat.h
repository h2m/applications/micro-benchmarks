#ifndef __DATA_CONTAINER_3MAT_H__
#define __DATA_CONTAINER_3MAT_H__

#include "data_container.h"
#include <cassert>
#include <cstdio>
#include <cstdlib>

#ifndef SIZE_M
#define SIZE_M 2000
#endif
#ifndef SIZE_N
#define SIZE_N 2000
#endif
#ifndef SIZE_K
#define SIZE_K 2000
#endif

#ifndef VAL_INIT_A
#define VAL_INIT_A 1.0
#endif
#ifndef VAL_INIT_B
#define VAL_INIT_B 1.0
#endif
#ifndef VAL_INIT_C
#define VAL_INIT_C 0.0
#endif

#ifndef MEM_SPACE_A
#define _SPACE_A h2m_atv_mem_space_large_cap
#else
#  if   MEM_SPACE_A == HBW || MEM_SPACE_A == hbw
#    define _SPACE_A h2m_atv_mem_space_hbw
#  elif MEM_SPACE_A == LARGE_CAP || MEM_SPACE_A == large_cap
#    define _SPACE_A h2m_atv_mem_space_large_cap
#  elif MEM_SPACE_A == LOW_LAT || MEM_SPACE_A == low_lat
#    define _SPACE_A h2m_atv_mem_space_low_lat
#  else
#    error "Invalid memory space for A"
#  endif
#endif

#ifndef MEM_SPACE_B
#define _SPACE_B h2m_atv_mem_space_large_cap
#else
#  if   MEM_SPACE_B == HBW || MEM_SPACE_B == hbw
#    define _SPACE_B h2m_atv_mem_space_hbw
#  elif MEM_SPACE_B == LARGE_CAP || MEM_SPACE_B == large_cap
#    define _SPACE_B h2m_atv_mem_space_large_cap
#  elif MEM_SPACE_B == LOW_LAT || MEM_SPACE_B == low_lat
#    define _SPACE_B h2m_atv_mem_space_low_lat
#  else
#    error "Invalid memory space for B"
#  endif
#endif

#ifndef MEM_SPACE_C
#define _SPACE_C h2m_atv_mem_space_large_cap
#else
#  if   MEM_SPACE_C == HBW || MEM_SPACE_C == hbw
#    define _SPACE_C h2m_atv_mem_space_hbw
#  elif MEM_SPACE_C == LARGE_CAP || MEM_SPACE_C == large_cap
#    define _SPACE_C h2m_atv_mem_space_large_cap
#  elif MEM_SPACE_C == LOW_LAT || MEM_SPACE_C == low_lat
#    define _SPACE_C h2m_atv_mem_space_low_lat
#  else
#    error "Invalid memory space for C"
#  endif
#endif

class settings_3mat_t {
    public:
        // dimension of matrices
        size_t size_M;
        size_t size_N;
        size_t size_K;

        // values for data init
        double val_A;
        double val_B;
        double val_C;

        // memory spaces
        h2m_alloc_trait_value_t mem_space_A;
        h2m_alloc_trait_value_t mem_space_B;
        h2m_alloc_trait_value_t mem_space_C;

        // traits
        h2m_alloc_trait_t *traits_A;
        h2m_alloc_trait_t *traits_B;
        h2m_alloc_trait_t *traits_C;
        int n_traits_A;
        int n_traits_B;
        int n_traits_C;

        // default constructor
        settings_3mat_t() {
            size_M = SIZE_M;
            size_N = SIZE_N;
            size_K = SIZE_K;

            val_A = VAL_INIT_A;
            val_B = VAL_INIT_B;
            val_C = VAL_INIT_C;

            mem_space_A = _SPACE_A;
            mem_space_B = _SPACE_B;
            mem_space_C = _SPACE_C;

            traits_A = nullptr;
            traits_B = nullptr;
            traits_C = nullptr;

            n_traits_A = 0;
            n_traits_B = 0;
            n_traits_C = 0;
        }

        // copy constructor
        settings_3mat_t(const settings_3mat_t& other) {
            size_M = other.size_M;
            size_N = other.size_N;
            size_K = other.size_K;

            val_A = other.val_A;
            val_B = other.val_B;
            val_C = other.val_C;

            mem_space_A = other.mem_space_A;
            mem_space_B = other.mem_space_B;
            mem_space_C = other.mem_space_C;

            traits_A = other.traits_A;
            traits_B = other.traits_B;
            traits_C = other.traits_C;

            n_traits_A = other.n_traits_A;
            n_traits_B = other.n_traits_B;
            n_traits_C = other.n_traits_C;
        }

        ~settings_3mat_t()
        {
            if (traits_A) delete traits_A;
            if (traits_B) delete traits_B;
            if (traits_C) delete traits_C;
        }

        void update_traits(int *n_traits, h2m_alloc_trait_t **traits, int n_new_traits, h2m_alloc_trait_t *new_traits)
        {
            // free old traits if existing
            if (*traits) {
                delete[] *traits;
                *traits = nullptr;
                *n_traits = 0;
            }

            // set / copy-in new traits
            *traits = new h2m_alloc_trait_t[n_new_traits];
            memcpy(*traits, new_traits, sizeof(h2m_alloc_trait_t)*n_new_traits);
            *n_traits = n_new_traits;
        }

        void update_traits_A(int n_traits, h2m_alloc_trait_t *traits) { update_traits(&this->n_traits_A, &this->traits_A, n_traits, traits); }
        void update_traits_B(int n_traits, h2m_alloc_trait_t *traits) { update_traits(&this->n_traits_B, &this->traits_B, n_traits, traits); }
        void update_traits_C(int n_traits, h2m_alloc_trait_t *traits) { update_traits(&this->n_traits_C, &this->traits_C, n_traits, traits); }
};

class data_container_3mat_t : public data_container_t {

    public:
        settings_3mat_t sett;
        double *A;
        double *B;
        double *C;

        void init(void *args) {
            // option to specify some parameters at run-time
            char *tmp = nullptr;
            tmp = nullptr; tmp = std::getenv("SIZE_M"); if(tmp) { this->sett.size_M = std::atol(tmp); }
            tmp = nullptr; tmp = std::getenv("SIZE_N"); if(tmp) { this->sett.size_N = std::atol(tmp); }
            tmp = nullptr; tmp = std::getenv("SIZE_K"); if(tmp) { this->sett.size_K = std::atol(tmp); }

            if(args) {
                // copy values to internal settings
                settings_3mat_t *other = static_cast<settings_3mat_t*>(args);
                this->sett = *other;
            }
        }

        void allocate(bool declare_alloc) {
            int err;

            // starting point: traits from settings
            int nt_A = this->sett.n_traits_A;
            int nt_B = this->sett.n_traits_B;
            int nt_C = this->sett.n_traits_C;
            h2m_alloc_trait_t *tA = this->sett.traits_A;
            h2m_alloc_trait_t *tB = this->sett.traits_B;
            h2m_alloc_trait_t *tC = this->sett.traits_C;

            // case: no traits specified -> use initial mem space for trait
            if(!this->sett.traits_A) {
                tA = new h2m_alloc_trait_t[1] { { .key = h2m_atk_pref_mem_space, .value = { .atv = this->sett.mem_space_A } } };
                nt_A = 1;
            }
            if(!this->sett.traits_B) {
                tB = new h2m_alloc_trait_t[1] { { .key = h2m_atk_pref_mem_space, .value = { .atv = this->sett.mem_space_B } } };
                nt_B = 1;
            }
            if(!this->sett.traits_C) {
                tC = new h2m_alloc_trait_t[1] { { .key = h2m_atk_pref_mem_space, .value = { .atv = this->sett.mem_space_C } } };
                nt_C = 1;
            }

            // allocate data
            this->declarations.push_back(h2m_declare_alloc_w_traits((void **)&(this->A), this->sett.size_M * this->sett.size_K * sizeof(double), nullptr, nullptr, &err, nt_A, tA));
            this->declarations.push_back(h2m_declare_alloc_w_traits((void **)&(this->B), this->sett.size_K * this->sett.size_N * sizeof(double), nullptr, nullptr, &err, nt_B, tB));
            this->declarations.push_back(h2m_declare_alloc_w_traits((void **)&(this->C), this->sett.size_M * this->sett.size_N * sizeof(double), nullptr, nullptr, &err, nt_C, tC));
            if (!declare_alloc) {
                // Still allocate the three arrays together as a coherent ensemble
                err = h2m_commit_allocs(&(this->declarations[this->declarations.size()-3]), 3);
                if(err != H2M_SUCCESS) {
                    fprintf(stderr, "Error: unable to allocate: h2m_commit_allocs returned %d\n", err);
                }
                this->declarations.pop_back();
                this->declarations.pop_back();
                this->declarations.pop_back();
            }

            // cleanup if temporary traits have been used
            if(!this->sett.traits_A) {
                delete[] tA;
            }
            if(!this->sett.traits_A) {
                delete[] tB;
            }
            if(!this->sett.traits_A) {
                delete[] tC;
            }
        }

        void data_init(bool parallel) {
            // for run parallel init (assumption: number of threads specified via env variables)
            #pragma omp parallel for schedule(static) if(parallel)
            for(int i = 0; i < this->sett.size_M; i++) {
                for(int j = 0; j < this->sett.size_K; j++) {
                    this->A[i*this->sett.size_K+j] = this->sett.val_A;
                }
            }
            #pragma omp parallel for schedule(static) if(parallel)
            for(int i = 0; i < this->sett.size_K; i++) {
                for(int j = 0; j < this->sett.size_N; j++) {
                    this->B[i*this->sett.size_N+j] = this->sett.val_B;
                }
            }
            #pragma omp parallel for schedule(static) if(parallel)
            for(int i = 0; i < this->sett.size_M; i++) {
                for(int j = 0; j < this->sett.size_N; j++) {
                    this->C[i*this->sett.size_N+j] = this->sett.val_C;
                }
            }
        }

        bool check_data_placement() {
            int found_A = 0, found_B = 0, found_C = 0;
            h2m_alloc_info_t info_A = h2m_get_alloc_info(this->A, &found_A);
            h2m_alloc_info_t info_B = h2m_get_alloc_info(this->B, &found_B);
            h2m_alloc_info_t info_C = h2m_get_alloc_info(this->C, &found_C);
            assert(found_A==1);
            assert(found_B==1);
            assert(found_C==1);

            if(info_A.cur_mem_space != this->sett.mem_space_A) {
                return false;
            }
            if(info_B.cur_mem_space != this->sett.mem_space_B) {
                return false;
            }
            if(info_B.cur_mem_space != this->sett.mem_space_B) {
                return false;
            }

            // === DEBUG ===
            char str_space_A[64], str_space_B[64], str_space_C[64];
            h2m_alloc_trait_value_t_str(info_A.cur_mem_space, str_space_A);
            h2m_alloc_trait_value_t_str(info_B.cur_mem_space, str_space_B);
            h2m_alloc_trait_value_t_str(info_C.cur_mem_space, str_space_C);
            char str_partition_A[64], str_partition_B[64], str_partition_C[64];
            h2m_alloc_trait_value_t_str(info_A.cur_mem_partition, str_partition_A);
            h2m_alloc_trait_value_t_str(info_B.cur_mem_partition, str_partition_B);
            h2m_alloc_trait_value_t_str(info_C.cur_mem_partition, str_partition_C);

            printf("Placement A: mem_space=%30s, mem_partition=%s, nodemask=%lu\n", str_space_A, str_partition_A, info_A.nodemask);
            printf("Placement B: mem_space=%30s, mem_partition=%s, nodemask=%lu\n", str_space_B, str_partition_B, info_B.nodemask);
            printf("Placement C: mem_space=%30s, mem_partition=%s, nodemask=%lu\n", str_space_C, str_partition_C, info_C.nodemask);
            // === DEBUG ===
            return true;
        }

        void finalize() {
            if(this->A) {
                h2m_free(this->A);
                this->A = nullptr;
            }
            if(this->B) {
                h2m_free(this->B);
                this->B = nullptr;
            }
            if(this->C) {
                h2m_free(this->C);
                this->C = nullptr;
            }
        }

        std::string get_id() {
            std::string ret =
                "data_container_3mat_" +
                std::to_string(this->sett.size_M) + "_" +
                std::to_string(this->sett.size_N) + "_" +
                std::to_string(this->sett.size_K) + "_" +
                std::to_string(this->sett.mem_space_A) + "_" +
                std::to_string(this->sett.mem_space_B) + "_" +
                std::to_string(this->sett.mem_space_C);
            return ret;
        }
};

#endif // __DATA_CONTAINER_3MAT_H__
