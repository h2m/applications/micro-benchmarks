#ifndef __KERNEL_MXNXK_TRANSPOSEDB_H__
#define __KERNEL_MXNXK_TRANSPOSEDB_H__

#include "kernel.h"
#include "kernel_MxNxK.h"
#include "data_container.h"
#include "data_container_3mat.h"
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>

#ifndef DO_BASELINE
#define DO_BASELINE 0
#endif

class kernel_MxNxK_transposedB_t : public kernel_MxNxK_t {

    public:
        void set_traits_for_data() {
#if DO_BASELINE
            data_container_3mat_t *dco = this->dc;
            set_3mat_traits(0, dco->sett.mem_space_A, 0, dco->sett.mem_space_B, 0, dco->sett.mem_space_C);
#else
            const double prio_A = 1.;
            const h2m_alloc_trait_value_t mem_space_A = h2m_atv_mem_space_hbw;
            const double prio_B = 10.;
            const h2m_alloc_trait_value_t mem_space_B = h2m_atv_mem_space_low_lat;
            const double prio_C = 5.;
            const h2m_alloc_trait_value_t mem_space_C = h2m_atv_mem_space_hbw;

            set_3mat_traits(prio_A, mem_space_A, prio_B, mem_space_B, prio_C, mem_space_C);
#endif // DO_BASELINE
        }
        
        int run(bool parallel) {
            settings_3mat_t *ds = &(this->dc->sett);

            // dummy used to avoid optimizing out loop
            volatile int ctr_reps = 0;

            for(int r = 0; r < this->sett.n_reps; r++) {
                ctr_reps += r;
                #pragma omp parallel for schedule(static) if(parallel)
                for(int i = 0; i < ds->size_M; i++) {
                    for(int j = 0; j < ds->size_N; j++) {
                        double tmp = 0;
                        for(int k = 0; k < ds->size_K; k++) {
                            tmp += dc->A[i*ds->size_K+k] * dc->B[k*ds->size_N+j];
                        }
                        dc->C[i*ds->size_N+j]=tmp;
                    }
                }
            }

            return ctr_reps;
        }

        std::string get_id() {
            std::string ret = "kernel_MxNxK-transposedB_rep_" + std::to_string(this->sett.n_reps);
            return ret;
        }
};

#endif // __KERNEL_MXNXK_TRANSPOSEDB_H__
