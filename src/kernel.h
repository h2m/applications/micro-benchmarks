#ifndef __KERNEL_H__
#define __KERNEL_H__

#include "data_container.h"
#include <string>

class kernel_t {

    public:
        virtual void        init(data_container_t *dc, void *args) = 0;
        virtual void        set_traits_for_data() = 0;
        virtual int         run(bool parallel) = 0;
        virtual bool        verify() = 0;
        virtual std::string get_id() = 0;
        virtual void        finalize() = 0;
        virtual            ~kernel_t() {}
};

#endif /* __KERNEL_H__ */
