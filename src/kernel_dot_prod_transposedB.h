#ifndef __KERNEL_DOT_PROD_TRANSPOSEDB_H__
#define __KERNEL_DOT_PROD_TRANSPOSEDB_H__

#include "kernel.h"
#include "kernel_dot_prod.h"
#include "data_container.h"
#include "data_container_3mat.h"
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>

#ifndef DO_BASELINE
#define DO_BASELINE 0
#endif

class kernel_dot_prod_transposedB_t : public kernel_dot_prod_t {

    public:
        void set_traits_for_data() {
#if DO_BASELINE
            data_container_3mat_t *dco = this->dc;
            set_3mat_traits(0, dco->sett.mem_space_A, 0, dco->sett.mem_space_B, 0, dco->sett.mem_space_C);
#else
            const double prio_A = 1.;
            const h2m_alloc_trait_value_t mem_space_A = h2m_atv_mem_space_hbw;
            const double prio_B = 10.;
            const h2m_alloc_trait_value_t mem_space_B = h2m_atv_mem_space_low_lat;
            const double prio_C = 5.;
            const h2m_alloc_trait_value_t mem_space_C = h2m_atv_mem_space_hbw;

            set_3mat_traits(prio_A, mem_space_A, prio_B, mem_space_B, prio_C, mem_space_C);
#endif // DO_BASELINE
        }

        int run(bool parallel) {
            settings_3mat_t *ds = &(this->dc->sett);
            // ensure that we only work with square matrixes
            assert(ds->size_M == ds->size_N);
            assert(ds->size_M == ds->size_K);

            // dummy used to avoid optimizing out loop
            volatile long keep = 0;
            volatile long ctr_iter = 0;
            volatile long ctr_reps = 0;

            if(this->sett.stride == 1) { // necessary as compiler might optimize more here
                for(int r = 0; r < this->sett.n_reps; r++) {
                    ctr_reps += r;
                    #pragma omp parallel for schedule(static) if(parallel)
                    for(int i = 0; i < ds->size_M; i++) {
                        for(int j = 0; j < ds->size_N; j++) {
                            // NOTE: Array B is used in a strided fashion here (Reason: we want some variety in access patterns)
                            dc->C[i*ds->size_N+j] += dc->A[i*ds->size_N+j] * dc->B[j*ds->size_N+i];
                        }
                    }
                }
            } else {
                // ensure the benchmark does roughly the same amount of accesses compared to stride=1
                for(int r = 0; r < this->sett.n_reps; r++) {
                    ctr_reps += r;
                    for(int it = 0; it < this->sett.stride; it++) {
                        ctr_iter += it;
                        #pragma omp parallel for schedule(static) if(parallel)
                        for(int i = 0; i < ds->size_M; i++) {
                            for(int j = 0; j < ds->size_N; j+=this->sett.stride) {
                                // NOTE: Array B is used in a strided fashion here (Reason: we want some variety in access patterns)
                                dc->C[i*ds->size_N+j] += dc->A[i*ds->size_N+j] * dc->B[j*ds->size_N+i];
                            }
                        }
                    }
                }
            }

            keep = ctr_iter + ctr_reps;
            return (int)keep;
        }

        std::string get_id() {
            std::string ret = "kernel_dot-product-transposedB_stride_" + std::to_string(this->sett.stride) +  "_rep_" + std::to_string(this->sett.n_reps);
            return ret;
        }
};

#endif // __KERNEL_DOT_PROD_TRANSPOSEDB_H__
