#ifndef __KERNEL_DOT_PROD_H__
#define __KERNEL_DOT_PROD_H__

#include "kernel.h"
#include "data_container.h"
#include "data_container_3mat.h"
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>

#ifndef N_REPS
#define N_REPS 1
#endif

#ifndef STRIDE
#define STRIDE 1
#endif

#ifndef DO_BASELINE
#define DO_BASELINE 0
#endif

class settings_dot_prod_t {
    public:
        int n_reps;
        int stride;

        // default constructor
        settings_dot_prod_t () {
            n_reps = N_REPS;
            stride = STRIDE;
        }

        // copy constructor
        settings_dot_prod_t(const settings_dot_prod_t& other) {
            n_reps = other.n_reps;
            stride = other.stride;
        }
};

class kernel_dot_prod_t : public kernel_t {

    protected:
        data_container_3mat_t *dc;
        settings_dot_prod_t sett;

        void set_3mat_traits(double prio_A, h2m_alloc_trait_value_t mem_space_A,
                             double prio_B, h2m_alloc_trait_value_t mem_space_B,
                             double prio_C, h2m_alloc_trait_value_t mem_space_C)
        {
            data_container_3mat_t *dco = this->dc;

            h2m_alloc_trait_t traits_A[2] = { { h2m_atk_pref_mem_space, { .atv = mem_space_A} }, { h2m_atk_access_prio, { .dbl = prio_A } } };
            dco->sett.update_traits_A(2, traits_A);

            h2m_alloc_trait_t traits_B[2] = { { h2m_atk_pref_mem_space, { .atv = mem_space_B} }, { h2m_atk_access_prio, { .dbl = prio_B } } };
            dco->sett.update_traits_B(2, traits_B);

            h2m_alloc_trait_t traits_C[2] = { { h2m_atk_pref_mem_space, { .atv = mem_space_C} }, { h2m_atk_access_prio, { .dbl = prio_C } } };
            dco->sett.update_traits_C(2, traits_C);

            // update traits for data structures (might not be successful before allocation but should not hurt)
            h2m_update_traits(dco->A, dco->sett.size_M * dco->sett.size_K * sizeof(double), dco->sett.n_traits_A, dco->sett.traits_A, 1);
            h2m_update_traits(dco->B, dco->sett.size_K * dco->sett.size_N * sizeof(double), dco->sett.n_traits_B, dco->sett.traits_B, 1);
            h2m_update_traits(dco->C, dco->sett.size_M * dco->sett.size_N * sizeof(double), dco->sett.n_traits_C, dco->sett.traits_C, 1);
        }

    public:
        void init(data_container_t *dc, void *args) {
            this->dc = static_cast<data_container_3mat_t *>(dc);

            // option to specify some parameters at run-time
            char *tmp = nullptr;
            tmp = nullptr; tmp = std::getenv("STRIDE"); if(tmp) { this->sett.stride = std::atoi(tmp); }

            if(args) {
                // copy values to internal settings
                settings_dot_prod_t *other = static_cast<settings_dot_prod_t*>(args);
                this->sett = *other;
            }
        }

        void set_traits_for_data() {
#if DO_BASELINE
            data_container_3mat_t *dco = this->dc;
            set_3mat_traits(0, dco->sett.mem_space_A, 0, dco->sett.mem_space_B, 0, dco->sett.mem_space_C);
#else
            const double prio_A = 1.5;
            const h2m_alloc_trait_value_t mem_space_A = h2m_atv_mem_space_hbw;
            const double prio_B = 1.5;
            const h2m_alloc_trait_value_t mem_space_B = h2m_atv_mem_space_hbw;
            const double prio_C = 5.;
            const h2m_alloc_trait_value_t mem_space_C = h2m_atv_mem_space_hbw;

            set_3mat_traits(prio_A, mem_space_A, prio_B, mem_space_B, prio_C, mem_space_C);
#endif // DO_BASELINE
        }

        int run(bool parallel) {
            settings_3mat_t *ds = &(this->dc->sett);
            // ensure that we only work with square matrixes
            assert(ds->size_M == ds->size_N);
            assert(ds->size_M == ds->size_K);

            // dummy used to avoid optimizing out loop
            volatile long keep = 0;
            volatile long ctr_iter = 0;
            volatile long ctr_reps = 0;

            if(this->sett.stride == 1) { // necessary as compiler might optimize more here
                for(int r = 0; r < this->sett.n_reps; r++) {
                    ctr_reps += r;
                    #pragma omp parallel for schedule(static) if(parallel)
                    for(int i = 0; i < ds->size_M; i++) {
                        for(int j = 0; j < ds->size_N; j++) {
                            dc->C[i*ds->size_N+j] += dc->A[i*ds->size_N+j] * dc->B[i*ds->size_N+j];
                        }
                    }
                }
            } else {
                // ensure the benchmark does roughly the same amount of accesses compared to stride=1
                for(int r = 0; r < this->sett.n_reps; r++) {
                    ctr_reps += r;
                    for(int it = 0; it < this->sett.stride; it++) {
                        ctr_iter += it;
                        #pragma omp parallel for schedule(static) if(parallel)
                        for(int i = 0; i < ds->size_M; i++) {
                            for(int j = 0; j < ds->size_N; j+=this->sett.stride) {
                                dc->C[i*ds->size_N+j] += dc->A[i*ds->size_N+j] * dc->B[i*ds->size_N+j];
                            }
                        }
                    }
                }
            }

            keep = ctr_iter + ctr_reps;
            return (int)keep;
        }

        bool verify() {
            settings_3mat_t *ds = &(this->dc->sett);
            double desired_val = ds->val_C + ds->val_A * ds->val_B * this->sett.n_reps * this->sett.stride;
            if (this->sett.n_reps > 0) {
                for(int i = 0; i < ds->size_M; i++) {
                    for(int j = 0; j < ds->size_N; j+=this->sett.stride) {
                        if(fabs(((double)dc->C[i*ds->size_N+j] - desired_val)) > 1e-3) {
                            printf("Error in matrix entry (%d,%d) expected:%f but value is %f\n", i, j, desired_val, dc->C[i*ds->size_N+j]);
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        std::string get_id() {
            std::string ret = "kernel_dot-product_stride_" + std::to_string(this->sett.stride) +  "_rep_" + std::to_string(this->sett.n_reps);
            return ret;
        }

        void finalize() { }
};

#endif // __KERNEL_DOT_PROD_H__
